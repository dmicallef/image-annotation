from django.conf.urls import include
from django.contrib import admin
from django.contrib.auth import logout
from django.urls import path

from apps.annotate.views import AnnotationExportView
from config.api import api

urlpatterns = [
    path("admin/", admin.site.urls, name="admin"),
    path("logout/", logout, {"next_page": "/"}, name="logout"),
    path("api/", include(api.urls)),
    path("export_csv/", AnnotationExportView.as_view(), name="export_csv"),
    path("api-auth/", include("rest_framework.urls", namespace="rest_framework")),
]
