from rest_framework import routers

from apps.annotate.views import (
    AnnotationImageViewSet,
    AnnotationViewSet,
    ProjectViewSet,
)
from apps.users.views import UserViewSet

# Settings
api = routers.DefaultRouter()
api.trailing_slash = '/?'

# Users API
api.register(r'users', UserViewSet)
api.register(r'projects', ProjectViewSet, basename='project')
api.register(r'images', AnnotationImageViewSet, basename='annotation_image')
api.register(r'annotate', AnnotationViewSet, basename='annotations')
