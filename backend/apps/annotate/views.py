from csv_export.views import CSVExportView
from django.utils import timezone
from rest_framework import viewsets

from apps.annotate.models import Annotation, AnnotationImage
from apps.annotate.serializers import (
    AnnotationImageSerializer,
    AnnotationSerializer,
    ProjectSerializer,
)


class ProjectViewSet(viewsets.ModelViewSet):
    serializer_class = ProjectSerializer

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def get_queryset(self):
        return self.request.user.projects.all()


class AnnotationImageViewSet(viewsets.ModelViewSet):
    serializer_class = AnnotationImageSerializer

    def get_queryset(self):
        return AnnotationImage.objects.filter(project__user=self.request.user)


class AnnotationViewSet(viewsets.ModelViewSet):
    serializer_class = AnnotationSerializer

    def get_queryset(self):
        image_id = self.request.query_params.get('image')
        queryset = Annotation.objects.filter(image__project__user=self.request.user)
        if image_id:
            queryset = queryset.filter(image__id=image_id)
        return queryset


class AnnotationExportView(CSVExportView):
    model = Annotation
    fields = ('image__name', 'coordinates')
    header = False
    specify_separator = False

    def get_queryset(self):
        queryset = super().get_queryset().filter(image__project__user=self.request.user)
        return queryset

    def get_filename(self, queryset):
        return 'annotation-export-{!s}.csv'.format(timezone.now())
