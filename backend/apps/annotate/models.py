from django.conf import settings
from django.db import models
from django.db.models import JSONField
from django.utils.translation import gettext_lazy as _


class AbstractTrackedModel(models.Model):
    class Meta:
        abstract = True

    ctime = models.DateTimeField(auto_now_add=True, verbose_name=_("Creation time"))
    mtime = models.DateTimeField(auto_now=True, verbose_name=_("Update time"))

    def save(self, *args, **kwargs):
        update_fields = kwargs.get("update_fields")

        if isinstance(update_fields, list) and "mtime" not in update_fields:
            update_fields.append("mtime")
        super().save(*args, **kwargs)


class Project(AbstractTrackedModel):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.PROTECT, related_name="projects",
    )
    name = models.CharField(_("Project Name"), max_length=255)

    def __str__(self):
        return self.name

    def __repr__(self):
        return f'<Project: {self.id} {self.name}>'


class AnnotationImage(AbstractTrackedModel):
    project = models.ForeignKey(
        Project, on_delete=models.CASCADE, related_name="images"
    )
    name = models.CharField(_("Image Filename"), max_length=255)
    image = models.ImageField(upload_to='uploads/', max_length=255)

    def __str__(self):
        return self.name

    def __repr__(self):
        return f'<Image: {self.id} {self.name}>'


class Annotation(AbstractTrackedModel):
    VEHILE_CHOICES = (
        ("car", "car"),
        ("bus", "bus"),
        ("bike", "bike"),
        ("autorickshaw", "autorickshaw"),
    )
    image = models.ForeignKey(
        AnnotationImage, on_delete=models.CASCADE, related_name="annotations"
    )
    object_type = models.CharField(
        _("Vehicle Type"), choices=VEHILE_CHOICES, null=False, max_length=25
    )
    coordinates = JSONField(null=True)

    def __str__(self):
        return self.id

    def __repr__(self):
        return f'<Annotation: {self.id} {self.object_type}>'
