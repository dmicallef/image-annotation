from rest_framework import serializers

from apps.annotate.models import Annotation, AnnotationImage, Project


class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = ['id', 'user', 'name']
        extra_kwargs = {
            'id': {'read_only': True},
            'user': {'read_only': True},
        }


class AnnotationImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = AnnotationImage
        fields = ['id', 'project', 'name', 'image']


class AnnotationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Annotation
        fields = ['image', 'object_type', 'coordinates']
