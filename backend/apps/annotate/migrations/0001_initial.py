# Generated by Django 4.0.3 on 2022-03-04 21:32

import django.db.models.deletion
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Project',
            fields=[
                (
                    'id',
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name='ID',
                    ),
                ),
                (
                    'ctime',
                    models.DateTimeField(
                        auto_now_add=True, verbose_name='Creation time'
                    ),
                ),
                (
                    'mtime',
                    models.DateTimeField(auto_now=True, verbose_name='Update time'),
                ),
                ('name', models.CharField(max_length=255, verbose_name='Project Name')),
                (
                    'user',
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.PROTECT,
                        related_name='projects',
                        to=settings.AUTH_USER_MODEL,
                    ),
                ),
            ],
            options={'abstract': False,},
        ),
        migrations.CreateModel(
            name='AnnotationImage',
            fields=[
                (
                    'id',
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name='ID',
                    ),
                ),
                (
                    'ctime',
                    models.DateTimeField(
                        auto_now_add=True, verbose_name='Creation time'
                    ),
                ),
                (
                    'mtime',
                    models.DateTimeField(auto_now=True, verbose_name='Update time'),
                ),
                (
                    'name',
                    models.CharField(max_length=255, verbose_name='Image Filename'),
                ),
                ('image', models.FileField(upload_to='')),
                (
                    'project',
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name='images',
                        to='annotate.project',
                    ),
                ),
            ],
            options={'abstract': False,},
        ),
        migrations.CreateModel(
            name='Annotation',
            fields=[
                (
                    'id',
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name='ID',
                    ),
                ),
                (
                    'ctime',
                    models.DateTimeField(
                        auto_now_add=True, verbose_name='Creation time'
                    ),
                ),
                (
                    'mtime',
                    models.DateTimeField(auto_now=True, verbose_name='Update time'),
                ),
                (
                    'object_type',
                    models.CharField(
                        choices=[
                            ('car', 'car'),
                            ('bus', 'bus'),
                            ('bike', 'bike'),
                            ('autorickshaw', 'autorickshaw'),
                        ],
                        max_length=25,
                        verbose_name='Vehicle Type',
                    ),
                ),
                ('coordinates', models.JSONField(null=True)),
                (
                    'annotation',
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name='annotations',
                        to='annotate.annotationimage',
                    ),
                ),
            ],
            options={'abstract': False,},
        ),
    ]
