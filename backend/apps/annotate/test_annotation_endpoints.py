import pytest


@pytest.mark.django_db
def test_projects_endpoint(authed_api_client, api_request_factory, annotation_user):
    projects_endpoint = '/api/projects'
    api_request_factory.post(
        projects_endpoint, {"user": annotation_user.id, "name": "Test"}
    )
    projects = api_request_factory.get(projects_endpoint)
    assert projects
    assert projects[0].name == "Test"
