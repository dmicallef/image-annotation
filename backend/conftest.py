import pytest
from pytest_factoryboy import register
from rest_framework.test import APIClient, APIRequestFactory

from apps.users.factories import AnnotationUserFactory

register(AnnotationUserFactory, 'annotation_user')


@pytest.fixture
def api_client():
    return APIClient()


@pytest.mark.django_db
@pytest.fixture
def authed_api_client(api_client, annotation_user):
    api_client.force_authenticate(annotation_user)
    return api_client


@pytest.fixture
def api_request_factory():
    return APIRequestFactory()
