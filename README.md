image_annotate
==============

A Django project with VueJS Frontend for image labelling.

Built using [cookiecutter-django-vue](https://github.com/vchaptsev/cookiecutter-django-vue).

## Getting Started with Image Annotation

1. Install [Docker](https://docs.docker.com/install/) and [Docker-Compose](https://docs.docker.com/compose/).

2. Make sure port 8000 is not occupied and start your docker containers with the following shell command:
  `docker-compose up --build`. This may take a while to complete. Nginx will return `502 Bad Gateway` response until all
   containers are running.

3. After it is done, you should be able to create an admin account. Open a terminal and navigate to the project directory. Create a user via:

`docker-compose run backend python manage.py createsuperuser`

4. Open a web browser and navigate to `localhost:8000`. It is possible to navigate through the API via DRF based UI at `localhost:8000/api`. You can Login (top-right) with credentials created in step 4. After logging in you will be able to view the REST API available. More information about the api below.

5. Alternatively you can create a new user, or login at `localhost:8000`

_NB: This is not a production env. Django is running in DEBUG mode._

## Backend API Description

### API Endpoints

The API is divided into four sections

 - users: `http://localhost:8000/api/users`
 - projects: `http://localhost:8000/api/projects`
 - images: `http://localhost:8000/api/images`
 - annotate: `http://localhost:8000/api/annotate`

NB: Most of the code for the API endpoints lives in `backend/apps` directory.

#### Users
Find at `http://localhost:8000/api/users`. User options are:

- Create/Register User
- GET List of users
- Login User
- Profile (GET details about logged in user)
- Password Change
- Reset Password

_NB: Most of this app is readily generated for us via cookiecutter-django-vue_

#### Projects

The goal of this endpoint is to create a new project. After logging in visit `http://localhost:8000/api/projects` to try out the different options.
- GET list of Projects related to the logged in user.
- POST a new project

#### Images

The goal here is to upload an image. Visit `http://localhost:8000/api/images` to upload an image or get a list of images.
- GET list of images for the logged in user.
- POST creates an image and saves it locally at `media/uploads` directory. Note that a cloud provider (e.g. s3) should be used in the future.

#### Annotate

The final part of the app, available at `http://localhost:8000/api/annotate`

- POST An annotation (image, vehicle type, and a list of coordinates)
- GET list of annotations

### Export to CSV

Navigate to `http://localhost:8000/export_csv`. The csv file containing all the annotations done by the logged in user will be downloaded automatically.

### Viewing uploaded images

Uploaded images can be viewed at `http://localhost:8000/media/<image-path>`
To get `<image-path>`, make a `GET` request to `http://localhost:8000/api/images`. The path will typically be `uploads/<image-name>`

### Testing

Pytest framework is used for testing. Black, Flake8 and iSort are all integrated. [FactoryBoy](https://github.com/FactoryBoy/factory_boy/tree/88458348b01c347e31cf1524a71482923d406a4f) is used to create model instances.

In a shell go to `backend` directory and run `pytest`
// Todo: Setup a docker container so tests run in it without needing to setup db

## Frontend Work

The frontend is written in Vue 2, and is split into 4 basic components;
  1. RegisterComponent (`http://localhost:8000/register)
  2. LoginComponent (http://localhost:8000/login)
  3. HomeComponent (http://localhost:8000)
  4. AnnotateComponent (Visited via Home)

[Bulma](bulma.io) and [Buefy](https://buefy.org/) is used for styling.

### Third-party Libraries
Annotation uses [vue-advanced-cropper](https://github.com/Norserium/vue-advanced-cropper/). This simplified the process
of creating annotation coordinates, and avoids the need of manually listening to mouse events.

Other libraries include `axios` as a client, and [vue-upload-drop-images](https://github.com/yudax42/vue-upload-drop-images) to easily
upload image files.

## Nginx
Nginx is used to serve resources. Configuration can be found under `nginx` directory. This config is auto-generated via cookiecutter.
