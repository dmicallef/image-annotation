import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import users from '@/store/services/users'
import auth from '@/store/modules/auth'

const store = new Vuex.Store({
  strict: true,
  state: {},
  mutations: {},
  modules: {
    users,
    auth
  }
})

export default store
