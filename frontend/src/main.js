import Vue from 'vue'
import store from '@/store'
import router from '@/router'

import axios from 'axios'
axios.defaults.xsrfCookieName = 'csrftoken'
axios.defaults.xsrfHeaderName = 'X-CSRFToken'

import Buefy from 'buefy'
import 'buefy/dist/buefy.css'

require('@/assets/main.scss');

import App from '@/App.vue'
import './registerServiceWorker'

Vue.config.productionTip = false
Vue.use(Buefy)

new Vue({
  router,
  store,
  beforeCreate(){
    store.dispatch('initialiseAuthStore');
  },
render: h => h(App)
}).$mount('#app')
