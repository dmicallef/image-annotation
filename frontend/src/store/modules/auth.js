import axios from 'axios'
import router from '@/router'

const state = {
  loggedIn: false,
  profile: {},
  validation: {email: true},
  authError: false
}

const getters = {
  isLoggedIn: (state) => state.loggedIn,
  authError: (state) => state.authError,
}

const mutations = {
  setAuthStore(state) {
    let storeItems = ['loggedIn']
    storeItems = storeItems.filter((key) => localStorage.getItem(key) !== null);
    const initState = storeItems.reduce((o, key) => (
      { ...o, [key]: JSON.parse(localStorage.getItem(key) || '0') }
    ), {});
    Object.assign(state, initState);
  },
  login (state) {
    state.loggedIn = true
    localStorage.setItem('loggedIn', true);
  },
  logout (state) {
    state.loggedIn = false
    localStorage.setItem('loggedIn', false);
  },
  setProfile (state, payload) {
    state.profile = payload
  },
  setValidationEmail (state, bool) {
    state.validation.email = bool
  },
  setAuthError (state, bool) {
    state.authError = bool
  }
}

const actions = {
  postLogin (context, payload) {
    return axios.post('/api/users/login/', payload)
      .then(response => {
        context.commit('login');
        router.push({ name: 'home' });
      })
      .catch(e => {
        context.commit('setAuthError', true)
        console.log(e)
      })
  },
  postRegister (context, payload) {
    return axios.post('/api/users/register/', payload)
      .then(response => {
        if (response.data.status === 210) {
          context.commit('setValidationEmail', false)
        } else {
          context.commit('setValidationEmail', true)
          router.push({ name: 'login' });
        }
      })
      .catch(e => { console.log(e) })
  },
  getProfile (context) {
    return axios.get('/api/users/profile')
      .then(response => {
        context.commit('login')
        context.commit('setProfile', response.data)
      })
      .catch(e => {
        context.commit('logout')
        console.log(e)
      })
  },
  getLogout(context) {
    return axios.get('api-auth/logout').then(response => {
        context.commit('logout');
        router.push({ name: 'login' });
      })
      .catch(e => {
        console.log(e)
      })
  },
  initialiseAuthStore(context){
    context.commit('setAuthStore');
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
