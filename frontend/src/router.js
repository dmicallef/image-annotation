import Vue from 'vue'
import VueRouter from 'vue-router'

import AnnotationComponent from '@/components/AnnotationComponent.vue'
import HomeComponent from '@/components/HomeComponent.vue'
import RegisterComponent from '@/components/RegisterComponent.vue'
import LoginComponent from '@/components/LoginComponent.vue'

// May want to load lazily
const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeComponent,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/annotate',
    name: 'annotation',
    component: AnnotationComponent,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/register/',
    name: 'register',
    component: RegisterComponent,
    meta: {
      hideForAuth: true
    }
  },
  {
    path: '/login/',
    name: 'login',
    component: LoginComponent,
    meta: {
      hideForAuth: true
    }
  },
]

Vue.use(VueRouter)
const router = new VueRouter({
  scrollBehavior (to, from, savedPosition) { return {x: 0, y: 0} },
  mode: 'history',
  routes
})

router.beforeEach((to, from, next) => {
  let isLoggedIn = JSON.parse(localStorage.getItem('loggedIn'));
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    if (!isLoggedIn) {
      next({ name: 'login' })

    } else {
      // Already authenticated
      next()
    }
  } else if (to.matched.some(record => record.meta.hideForAuth)) {
    if (isLoggedIn) {
      next({ name: 'home' })

    } else {
      next()

    }
  } else {
    // does not require auth
    next()
  }
})

export default router
